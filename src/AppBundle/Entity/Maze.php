<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="vivre_maze", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MazeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Maze
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $dim_x;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $dim_y;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $density;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $generated_matrix;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDimX()
    {
        return $this->dim_x;
    }

    /**
     * @param int $dim_x
     */
    public function setDimX(int $dim_x)
    {
        $this->dim_x = $dim_x;
    }

    /**
     * @return int
     */
    public function getDimY()
    {
        return $this->dim_y;
    }

    /**
     * @param int $dim_y
     */
    public function setDimY($dim_y)
    {
        $this->dim_y = $dim_y;
    }

    /**
     * @return int
     */
    public function getDensity()
    {
        return $this->density;
    }

    /**
     * @param int $density
     */
    public function setDensity($density)
    {
        $this->density = $density;
    }

    /**
     * @return string
     */
    public function getGeneratedMatrix()
    {
        return $this->generated_matrix;
    }

    /**
     * @param string $generated_matrix
     */
    public function setGeneratedMatrix($generated_matrix)
    {
        $this->generated_matrix = $generated_matrix;
    }
}
