// javascript maze

function testBeforeAddDb (cells) {
    let resultMaze = '';
    for (let i = 0; i < cells.length; i++) {
        for (let j = 0; j < cells[i].length; j++) {
            resultMaze += cells[i][j];
        }
        resultMaze += '<br/>';
    }

    $('#testBeforeAddDb').html(resultMaze);
}

function insertDb (dataMaze, dataInput) {
    $.ajax({
        url: $('#theMaze').data('url'),
        method: 'post',
        data: {
            dataMaze: JSON.stringify(dataMaze),
            dataInput: JSON.stringify(dataInput),
        }
    }).done(function(response) {
        console.log(response);
    });
}

function findPath(dataPath) {
    let maze = dataPath['maze'];
    if (typeof maze[dataPath['pos_ax']][dataPath['pos_ay']] === "undefined" || typeof maze[dataPath['pos_bx']][dataPath['pos_by']] === "undefined") {
        $('#mazeError')
            .html('Correct the keys in maze, for the two points A, B')
            .css({'color':'white', 'background-color':'red'});
    } else if (maze[dataPath['pos_ax']][dataPath['pos_ay']] == 'B' || maze[dataPath['pos_bx']][dataPath['pos_by']] == 'B') {
        $('#mazeError')
            .html('The two points A, B must be empty cells')
            .css({'color':'white', 'background-color':'red'});
    } else {

    }
}

function drawMaze(data) {

    let drawMaze = '';
    let new_width;
    let new_height;
    let totalCells;
    let totalCellsC;
    let totalCellsL;
    let random;
    let mazeLine = [];
    let maze = [];
    let cnt = -1;
    let cnt2 = -1;
    let element = $('#theMaze');
    let dataPath = [];

    totalCells = data['dim_x'] * data['dim_y'];

    totalCellsC = Math.ceil( ( data['brick_density'] / 100 ) * totalCells );
    totalCellsL = totalCells - totalCellsC;

    //console.log('totalCellsC: ' + totalCellsC + ', totalCellsL' + totalCellsL);

    for (let i = 0; i < data['dim_y']; i++) {
        for (let j = 0; j < data['dim_x']; j++) {
            random = Math.floor(Math.random() * 2);
            // adaug celule libere L (O)
            if (random === 0) {
                if (totalCellsL >= 1) {
                    drawMaze += '<div class="squareL"></div>';
                    mazeLine[++cnt] = 'O';
                    totalCellsL--;
                // daca nu mai am celule libere, adaug caramizi
                } else {
                    drawMaze += '<div class="squareC"></div>';
                    mazeLine[++cnt] = 'B';
                    totalCellsC--;
                }
            // adaug celule caramida C (B = brick)
            } else {
                if (totalCellsC >= 1) {
                    drawMaze += '<div class="squareC"></div>';
                    mazeLine[++cnt] = 'B';
                    totalCellsC--;
                // daca nu mai am caramizi, adaug celule libere
                } else {
                    drawMaze += '<div class="squareL"></div>';
                    mazeLine[++cnt] = 'O';
                    totalCellsL--;
                }
            }
        }
        drawMaze += '<div class="clear"></div>';
        maze[++cnt2] = mazeLine;
        mazeLine = [];
        cnt = -1;
    }

    new_width = data['dim_x'] * 20 + data['dim_x'] * 2;
    new_height = data['dim_y'] * 20 + data['dim_y'] * 2;

    if (new_width > 1000) {
        element.css({'width':new_width+'px'});
    }
    if (new_height > 800) {
        element.css({'height':new_height+'px'});
    }

    element.html( drawMaze );

    dataPath['pos_ax'] = data['pos_ax'];
    dataPath['pos_ay'] = data['pos_ay'];
    dataPath['pos_bx'] = data['pos_bx'];
    dataPath['pos_by'] = data['pos_by'];
    dataPath['maze'] = maze;

    testBeforeAddDb(maze);
    insertDb(maze, Object.entries(data));
    findPath(dataPath);
}

drawMaze(data);
