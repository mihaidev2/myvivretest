<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Maze;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $data = $this->get('maze_data_manager')->build($request);

        return $this->render('@App/Default/index.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function save(Request $request)
    {
        $dataMaze = $request->request->get('dataMaze');
        $dataInput = json_decode($request->request->get('dataInput'));

        $maze = new Maze();
        $maze->setDimX($dataInput[0][1]);
        $maze->setDimY($dataInput[1][1]);
        $maze->setDensity($dataInput[6][1]);
        $maze->setGeneratedMatrix($dataMaze);

        $em = $this->getDoctrine()->getManager();
        $em->persist($maze);
        $em->flush();

        return new JsonResponse([
            'code' => 202,
        ]);
    }
}
