<?php


namespace AppBundle\Services;


use Symfony\Component\HttpFoundation\Request;

class MazeDataManager
{
    /**
     * @param Request $request
     * @return array
     */
    public function build(Request $request)
    {
        return [
            'dim_x' => $request->request->get('dim_x', 6),
            'dim_y' => $request->request->get('dim_y', 5),
            'pos_ax' => $request->request->get('pos_ax', 2),
            'pos_ay' => $request->request->get('pos_ay', 2),
            'pos_bx' => $request->request->get('pos_bx', 2),
            'pos_by' => $request->request->get('pos_by', 6),
            'brick_density' => $request->request->get('brick_density', 50),
        ];
    }
}