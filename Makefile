.PHONY: export zip

export:
	mysqldump -hmyvivretest.local -umyvivre -pmyvivre myvivre > myvivre.sql

zip:
	git archive -o myvivre.zip HEAD